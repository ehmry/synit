#!/bin/sh
ROOTDIR=/home/tonyg/nobackup/build/postmarketos/chroot_rootfs_qemu-amd64
IMGDIR=/home/tonyg/nobackup/build/postmarketos/chroot_native/home/pmos/rootfs

BASEIMG=/tmp/pmos-qemu-amd64-base.qcow
TMPIMG=/tmp/pmos-qemu-amd64.qcow

while [ "$#" -gt "0" ]
do
    case "$1" in
        --clean)
            shift
            rm -f ${TMPIMG}
            ;;
        *)
            echo "Usage: $0 [--clean]" >&2
            exit 1
            ;;
    esac
done

if [ ! -f ${BASEIMG} ]
then
    qemu-img convert -f raw -O qcow2 ${IMGDIR}/qemu-amd64.img ${BASEIMG}
    qemu-img resize ${BASEIMG} 4G
fi
if [ ! -f ${TMPIMG} ]
then
   qemu-img create -f qcow2 -F qcow2 -b ${BASEIMG} ${TMPIMG}
fi

if [ -z "${DISPLAY:-}" ]
then
    displayopts="-display none"
else
    displayopts="-display gtk"
fi

qemu-system-x86_64 \
    -nodefaults \
    -kernel ${ROOTDIR}/boot/vmlinuz-lts \
    -initrd ${ROOTDIR}/boot/initramfs \
    -append 'console=tty1 console=ttyS0 PMOS_NO_OUTPUT_REDIRECT PMOS_FORCE_PARTITION_RESIZE video=1024x768@60' \
    -smp 48 \
    -m 1024 \
    -serial stdio \
    -drive file=${TMPIMG},format=qcow2,if=virtio \
    -device virtio-mouse-pci \
    -device virtio-keyboard-pci \
    -nic user,model=virtio-net-pci,hostfwd=tcp::2222-:22, \
    -vga virtio \
    -enable-kvm \
    -cpu host \
    ${displayopts}

#!/bin/sh

ROOTDEV=/dev/vda
CONSOLE=/dev/ttyAMA0
NEWROOT=/mnt

in_initramfs=true
if [ -f /proc/mounts ]
then
    in_initramfs=false
fi

if [ $in_initramfs = true ]
then
    echo "Mounting basics."
    mount -t devtmpfs none /dev
    mkdir /dev/pts
    mkdir /dev/shm
    mount -t devpts none /dev/pts
    mount -t proc none /proc
    mount -t sysfs none /sys
    mount -t tmpfs -o nodev,nosuid,noexec none /dev/shm
    mount -t tmpfs none /run

    # Docker wants this
    mount -t cgroup2 none /sys/fs/cgroup

    echo "Loading virtio modules."
    # The "virt" aarch64 qemu machine needs these to get user networking going
    modprobe virtio_pci
    modprobe virtio_net
    # And this is for the disk
    modprobe virtio_blk

    echo "Loading btrfs."
    modprobe btrfs

    echo "Attempting to mount ${ROOTDEV} on ${NEWROOT} ..."
    if mount ${ROOTDEV} ${NEWROOT}
    then
        echo "Mount successful."
    else
        echo "Mount unsuccessful. Building new root."
        (
            mkfs.btrfs ${ROOTDEV}
            mount ${ROOTDEV} ${NEWROOT}
        ) &

        echo "Configuring network."
        (
            ifconfig lo 127.0.0.1 up
            ifconfig eth0 up
            udhcpc -i eth0
        ) &

        wait

        apk --repositories-file /etc/apk/repositories -U --allow-untrusted \
            --root ${NEWROOT} --initdb add $(cat /package-list)
    fi

    for f in /etc/apk/repositories /init /package-list /sbin/syndicate-server /sbin/synit-pid1 /usr/bin/preserves-tool
    do
        echo "Updating $f."
        cp -a $f ${NEWROOT}$f
    done

    echo "Moving mount points."
    for mp in /dev /proc /sys /run
    do
        echo "Moving $mp --> ${NEWROOT}$mp"
        mount -o move $mp ${NEWROOT}$mp
    done

    echo "Setting up syndicate configuration."
    rm -rf ${NEWROOT}/etc/syndicate
    (cd ${NEWROOT} && tar -zxvf /etc_syndicate.tar.gz && mv etc_syndicate ${NEWROOT}/etc/syndicate)
    mkdir -p ${NEWROOT}/run/etc/syndicate
    mkdir -p ${NEWROOT}/usr/local/etc/syndicate

    echo "Setting up locking directories."
    mkdir -p ${NEWROOT}/var/lock/synit

    echo "Killing leftovers."
    kill -15 -1
    sleep 0.2
    kill -9 -1

    echo "Switching root."
    exec switch_root -c ${CONSOLE} ${NEWROOT} /init
else
    echo "Initializing with real rootfs."
fi

sync

# # setsid creates a "session", which allows job control to work
# exec setsid sh -c "exec sh -i <${CONSOLE} >${CONSOLE} 2>&1"

RUST_LOG=info exec /sbin/synit-pid1

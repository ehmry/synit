#!/bin/sh
docker buildx build -t synit-squid "$(dirname "$0")"
docker run --rm --name fetch-squid-cert synit-squid cat /etc/ssl/certs/squid-ca.pem \
       > squid-ca.pem
docker run -it --rm \
       -p 3127:3127 \
       -p 3128:3128 \
       -v /var/tmp/synit-squid-cache:/var/spool/squid \
       --name squid \
       synit-squid

---
title: 'System control commands'
---

## systemctl

Commands generally accept patterns to select subsets of the set of all
units.

 - list-units
 - list-sockets
 - list-timers
 - is-active (predicate over units)
 - is-failed
 - status
 - show
 - cat (introspection on source files for units)
 - help (this is interesting!)
 - list-dependencies

 - start
 - stop
 - reload ("please reload your configuration" - even though we are
   mostly reactive, most services won't be, and sometimes config in
   files exists etc)
 - restart (stop (if running) and then start (unconditionally))
 - try-restart (if running, stop and then start)
 - reload-or-restart, try-reload-or-restart
 - isolate (effectively set a new "supergoal", removing all others. nb `AllowIsolate` flag)
 - kill
 - clean (remove various cache/state/config/log data)
 - freeze, thaw
 - set-property (for us, change a config .pr file; no --runtime equivalent, though)
 - reset-failed (resets the state variables of the supervisor)
 - (several others)

Enable vs start.

Enable: `--system`, `--user`, `--runtime`, `--global`: various scopes for enabling a unit.

Container management.

Job management. (It has a job queue!)

Environment management for spawned services.

`daemon-reexec`, serializes state for an upgrade

System lifecycle: startup -> running -> (shutdown | (maintenance -?-> running))

At shutdown or sleep, "inhibitor locks" exist. The action can respect
or ignore these. Interactive shutdown/sleep requests default to
respecting; non-interactive, to ignoring.

`isolate` targets:
 - default
 - rescue
 - emergency
 - halt
 - poweroff
 - reboot
 - suspend
 - hibernate
 - hybrid-sleep
 - suspend-then-hibernate

`switch-root`

## Service unit configuration

https://www.freedesktop.org/software/systemd/man/systemd.service.html

Service templates: can take a single argument to become instantiated.

If a service unit config file doesn't exist, falls back to looking for
an init script and synthesizes a unit on the fly.

Default dependencies:
 - requires sysinit.target
 - starts after sysinit.target
 - starts after basic.target
 - conflicts with shutdown.target
 - shuts down before shutdown.target (?)

"slice units"? Oh ok, hierarchical supervision... and configuration of
whole branches of the tree.

Service lifecycle:
 - states: active, activating, deactivating, dead, inactive, starting, ... more?

Options:

 - Type (it's complicated)

---
title: 'Buses'
---

DBus - https://www.freedesktop.org/wiki/Software/dbus/

ubus (openwrt) - https://openwrt.org/docs/techref/ubus
 - https://git.openwrt.org/project/ubus.git
 - ACLs

plumber (plan9!) - http://man.cat-v.org/plan_9/4/plumber
 - data format "plumb" http://man.cat-v.org/plan_9/6/plumb
 - autostarts listeners on request

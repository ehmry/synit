#!/bin/sh

mount -t devtmpfs none /dev

mkdir /dev/pts
mount -t devpts none /dev/pts

mkdir /dev/shm
mount -t tmpfs -o nodev,nosuid,exec none /dev/shm
# ^ must be mounted "exec" to allow JIT for Squeak

mount -t proc none /proc

mount -t sysfs none /sys

mount -t tmpfs none /run

# Docker wants this
mount -t cgroup2 none /sys/fs/cgroup

mount -o rw,remount /

mkdir -p /etc/syndicate/user-settings
mkdir -p /run/etc/syndicate/core
mkdir -p /usr/local/etc/syndicate/core
mkdir -p /run/etc/syndicate/services
mkdir -p /usr/local/etc/syndicate/services

# This is a grody hack. We are replacing the infrastructure that sets up RNDIS, and in
# particular we no longer want to run the DHCP server that PostmarketOS runs by default on
# usb0. Instead we will be a DHCP client. (This was just easiest to set up.)
killall unudhcpd || true

exec /sbin/synit-pid1

all: run

.PHONY: syndicate-server synit-pid1 preserves-tool
.PHONY: all run boot clean veryclean veryveryclean

clean:
	rm -f initramfs-syndicate-system
	rm -f etc_syndicate.tar.gz

veryclean: clean
	rm -f disk.img
	rm -f syndicate-server synit-pid1 preserves-tool

veryveryclean: veryclean
	sudo rm -rf newroot

ALPINE_VERSION=v3.14
SYNDICATE_RS=~/src/syndicate-rs
SYNIT_PID1=$(CURDIR)/../../synit-pid1
PRESERVES_TOOLS=~/src/preserves/implementations/rust

# vmlinuz-lts:
# 	wget http://dl-cdn.alpinelinux.org/alpine/$(ALPINE_VERSION)/releases/aarch64/netboot/vmlinuz-lts
# initramfs-lts:
# 	wget http://dl-cdn.alpinelinux.org/alpine/$(ALPINE_VERSION)/releases/aarch64/netboot/initramfs-lts

disk.img:
	qemu-img create -f qcow2 $@ 8G

initramfs-syndicate-system: pack-image.sh init.sh package-list syndicate-server synit-pid1 preserves-tool etc_syndicate.tar.gz
	sudo PACKAGES="$$(cat package-list)" ./pack-image.sh

etc_syndicate.tar.gz: etc_syndicate/*
	tar -zcvf $@ etc_syndicate/*

syndicate-server:
	$(MAKE) -C $(SYNDICATE_RS) aarch64-binary-release
	rsync -av $(SYNDICATE_RS)/target/aarch64-unknown-linux-musl/release/syndicate-server .

preserves-tool:
	$(MAKE) -C $(PRESERVES_TOOLS) aarch64-binary-release
	rsync -av $(PRESERVES_TOOLS)/target/aarch64-unknown-linux-musl/release/preserves-tool .

synit-pid1:
	$(MAKE) -C $(SYNIT_PID1) aarch64-binary-release
	rsync -av $(SYNIT_PID1)/target/aarch64-unknown-linux-musl/release/synit-pid1 .

# See https://superuser.com/questions/1397991/running-alpine-linux-on-qemu-arm-guests
run: initramfs-syndicate-system disk.img boot

boot:
	qemu-system-aarch64 \
	  -M virt -m 512M -cpu cortex-a57 \
	  -kernel vmlinuz-virt -initrd initramfs-syndicate-system \
	  -netdev user,id=unet,hostfwd=tcp::8022-:22 -device virtio-net-pci,netdev=unet \
	  -hda disk.img \
	  -nographic

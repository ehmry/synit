---
title: 'What does systemd do?'
---

 - ac-power: reports whether A/C power is connected
 - activate: do socket activation (setup sockets, exec() on activity)
 - analyze: "profile systemd", various renderings and checks of system state and config
 - ask-password: secure password entry for e.g. cryptsetup
 - backlight: saves/restores backlight brightness at shutdown/boot
 - basic
    - architecture: show some abstraction over current architecture
    - escape: string escaping for e.g. shell (blearghhh)
    - log: various kinds of logging (incl. syslog, journal, etc)
    - ratelimit: generic rate limiting datastructure and utilities
    - reboot: system call
    - util
       - in_initrd() predicate
    - virt: detects whether, and what kind of, virtualization is in effect
 - binfmt: registers binfmts with the kernel
 - boot: bootctl etc: communicate with EFI (?) about successful/failed boots
    - boot/efi: an actual EFI implementation?? can't be? maybe it's an EFI boot program. I don't know enough about this
 - busctl: introspect contents of D-Bus
 - cgls: "Recursively show control group contents"
 - cgroups-agent: sends a message to unix socket /run/systemd/cgroups-agent
 - cgtop: "Show top control groups by their resource usage"
 - core
    - automount
    - dbus-*, lots of dbus things, wonder what they all are
    - dynamic-user: dynamic user/group ID allocation for, like, temporary use?
    - efi-random: incorporate any random seed passed down from EFI
    - execute: execute programs. lots of code here!
    - job: a job/worker manager system
 - detect-virt: detect virtualization
 - firstboot
 - fsck: runs fsck
 - fstab-generator
 - getty-generator
 - hibernate-resume: a little initrd program that runs when restoring from hibernation!
 - home: manage "home areas"
 - hostname management
 - hwdb: a hardware database
 - id128: various identifiers
    - machine ID
    - boot ID
    - invocation ID
 - import: image importing for containers presumably
 - initctl: like telinit I guess
 - journal: journald, etc
 - kernel-install: when would this be used?
 - libsystemd: ?
 - libsystemd-network: implementations of network protocols?
    - dhcp, dhcp6
    - lldp
 - libudev: ?
 - login
 - machine: machined: "Manage registrations of local VMs and containers"
 - machine-id-setup: gives a machine a random persistent ID
 - modules-load: load configured modules (for explicit loading, rather than on demand)
 - mount: transient mounts
 - network: various network services e.g. dhcp, lldp, route management, wifi management
 - notify: "Notify the init system about service status updates"
 - nspawn: "Spawn a command or OS in a light-weight container"
 - nss-*
 - oom: userspace OOM killer
 - partition: partitioning tools
 - pstore: ?
 - random-seed: load/save random seed at boot/shutdown
 - resolve: a full DNS resolver
    - or? `systemd-resolved` is an interesting take on non-daemonless DNS service for processes
      on the machine: in particular it is able to do split DNS right, with ~routes advertising
      their ability to resolve specific subsets of DNS space. Apparently DBus can be used to
      get rich queries and control things, and there's a listener on a localhost address
      (127.0.0.53?) for unmodified processes to use. Tailscale makes use of the split DNS to
      get things like resolution of the private names. Presumably you can say "hey give these
      names to me and *only* me" as well as "I am able to resolve these names, but if others
      also claim to be able to, go ahead and try things in parallel"? Unclear.
 - rfkill: radio power control
 - run: "Run the specified command in a transient scope or service"
 - shared
 - shutdown
 - sleep: enter sleep, hibernate, or both (?)
 - sysctl: "Applies kernel sysctl settings"
 - sysusers: creates system user accounts
 - timedate, timesync: clock management, NTP etc
 - tmpfiles: creates hierarchy under volatile/tmpfs areas of the filesystem (!) eg /run, /tmp etc
 - udev (!!)
 - vconsole: setup virtual consoles for e.g. utf8 mode

Unit names: "plain", "template", "instance" (of template).

Unit files:
 - have a "scope" - "system", "global", "user". What does it mean?
 - have a "type":
        UNIT_SERVICE
        UNIT_MOUNT
        UNIT_SWAP
        UNIT_SOCKET
        UNIT_TARGET
        UNIT_DEVICE
        UNIT_AUTOMOUNT
        UNIT_TIMER
        UNIT_PATH
        UNIT_SLICE
        UNIT_SCOPE
 - have a "state":
        UNIT_ACTIVE
        UNIT_RELOADING
        UNIT_INACTIVE
        UNIT_FAILED
        UNIT_ACTIVATING
        UNIT_DEACTIVATING
        UNIT_MAINTENANCE

Lots of code spent on argv parsing

# Structuring the System Layer with Dataspaces

In this project, Dataspace theory will be co-developed with new system
layer software in the setting of the user interface to and management
of a cellular telephone.

The expected project outcomes are:

 1. A capability-based security model for Dataspaces
 2. A proof-of-concept dataspace-based system layer for a mobile phone
 3. A qualitative evaluation of the suitability of the Dataspace model
    as structuring principle for system layers

The following artifacts are expected to realise the outcomes of the
project:

 1. Documents describing the theory of capabilities as applied to Dataspaces
 2. Dataspace implementations reflecting this theory
 3. Documents analysing the elements of a selection of existing system layers
 4. Protocols capturing a synthesis of system layer behaviours, based on the analysis
 5. A system layer implementation making use of Dataspaces
 6. Associated demos and case studies
 7. A website gathering together all project material
 8. Software releases and developer-focused documentation

## Subtasks

 1. ✓✓ Project infrastructure setup
    - Project website up under the umbrella of https://syndicate-lang.org/
    - Project blog created
    - Software Bill of Materials

 2. ✓✓ Capability theory
    - Description of Dataspace interaction model augmented with
      “data-capabilities” authorizing interaction within a dataspace

 3. ✓✓ Rust Dataspace infrastructure
    - Dataspace implementation for dataspace-based Rust programming
    - Implementation of Dataspace protocol in Rust

 4. ✓✓ Smalltalk Dataspace infrastructure
    - Dataspace implementation for dataspace-based Squeak Smalltalk programming
    - Implementation of Dataspace protocol in Smalltalk

 5. ✓✓ Demo of capability-secure Dataspace interaction

 6. ✓✓ PostmarketOS (pmOS) infrastructure
    - pmOS booting on cellphone to modified environment
    - Scripts for packaging of on-phone artifacts

 7. Demo exercising phone infrastructure

 8. ✓✓ Dataspace Protocol specification
    - Draft specification of interaction protocols for secure
      inter-process and inter-machine dataspace interaction

 9. ✓✓ Dataspace “message bus”
    - Standalone on-phone generic Dataspace “message bus” service for
      coordinating system-layer activity
    - Implements Dataspace Protocol

 10. ✓✓ Analysis of existing system-layers
     - Survey of a selection of system-layer components
     - Classification and categorisation of functionality

 11. ✓✓ Synthesis of system-layer functionality
     - Development and description of Dataspace interaction protocols
       capturing system-layer features

 12. ✓✓ Prototyping of Dataspace system layer
     - Development and description of mobile-phone-specific Dataspace
       interaction protocols
     - Development of generic system-layer components
     - Development of mobile-phone-specific components

 13. Demo exercising realised system layer

 14. Security & accessibility review
     - Security review of items 8, 9 and 12 would be most worthwhile.
     - Accessibility review of web content is likely to be trivial;
       review of the UI content of items 13 and 7 may be worthwhile
       (though user interface is not a focus of the project).

# Contributor: Tony Garnock-Jones <tonyg@leastfixedpoint.com>
# Maintainer: Tony Garnock-Jones <tonyg@leastfixedpoint.com>
pkgname=synit-config
pkgver=0.0.6$(cat GITVERSION)
pkgrel=0
pkgdesc="synit system layer configuration"
url="https://synit.org/"
arch="noarch"
license="GPLv3"
depends="
        synit-pid1
        syndicate-server
        syndicate-schemas
        preserves-schemas
        py3-syndicate

        py3-pyroute2

        eudev
        nftables
        openssh
        rsync
        wpa_supplicant
"
subpackages="
        $pkgname-docker:docker
"
makedepends="
        rsync
"
builddir="$srcdir/"
options="!check"
install="$pkgname.post-deinstall"

# This allows us to stomple on /sbin/init, which is owned by the busybox package
replaces="busybox"

build() {
    :
}

package() {
    mkdir -p "$pkgdir"
    rsync -a "$startdir/files/." "$pkgdir/."

    mkdir -p "$pkgdir/usr/share/synit/schemas"
    ls -laR $startdir/protocols
    cp "$startdir/protocols/schema-bundle.bin" "$pkgdir/usr/share/synit/schemas/schema-bundle.prb"
    cp "$startdir/protocols/schemas/"*.prs "$pkgdir/usr/share/synit/schemas/."

    ln -sf /sbin/synit-init.sh "$pkgdir/sbin/init"
    mkdir -p "$pkgdir/var/lock/synit"
}

docker() {
    depends="$pkgname"
    description="Synit startup for Docker"
    install_if="$pkgname docker"

    amove etc/syndicate/services/docker.pr
}

import sys
import os
import preserves.schema
import hashlib

from syndicate import patterns as P, relay, turn, dataspace, Symbol, canonicalize, stringify
from syndicate.actor import find_loop
from syndicate.during import During

try:
    schemas = preserves.schema.load_schema_file('/usr/share/synit/schemas/schema-bundle.prb')
except:
    schemas = preserves.schema.load_schema_file('/home/tonyg/src/synit/protocols/schema-bundle.bin')
userSettings = schemas.userSettings

def digest(item):
    return hashlib.sha1(canonicalize(item)).hexdigest()

@relay.service(name='user_settings_daemon', debug=False)
@During().add_handler
def main(args):
    config = args[Symbol('config')].embeddedValue
    settingsDir = args[Symbol('settingsDir')]

    def assert_item(item):
        filename = os.path.join(settingsDir, digest(item) + '.pr')
        turn.log.info(f'Asserting: {item} --> {filename}')
        with open(filename, 'wt') as f:
            f.write(stringify(item, indent=2) + '\n')

    def retract_item(item):
        filename = os.path.join(settingsDir, digest(item) + '.pr')
        turn.log.info(f'Retracting: {item} --> {filename}')
        try:
            os.unlink(filename)
        except FileNotFoundError:
            pass

    def perform_action(action):
        action._accept({
            'assert': assert_item,
            'retract': retract_item,
        })

    @dataspace.during(config, P.bind(P.quote(userSettings.CommandRPC(P.u_, P.u_))))
    def handle_command_rpc(c):
        c = userSettings.CommandRPC.try_decode(c)
        if c is None: return
        perform_action(c.action)
        turn.publish(c.reply, userSettings.CommandReply())

    @dataspace.on_message(config, P.bind(P.quote(userSettings.CommandEvent(P.u_))))
    def handle_command_event(c):
        c = userSettings.CommandEvent.try_decode(c)
        if c is None: return
        perform_action(c.action)

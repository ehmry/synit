Per <https://en.wikipedia.org/wiki/Runlevel>:

 - Standard runlevels

   - 0: off
   - 1: single user mode
   - 6: reboot

 - LSB runlevels:

   - 0: off
   - 1: single user mode
   - 2: multi-user mode
   - 3: multi-user with networking
   - 4: (undefined, available for local use)
   - 5: full mode, same as 3 plus display manager
   - 6: reboot

 - Other systems are similar, with broad axes being:
   - "single-user" vs "multi-user" vs "nothing at all (off or rebooting)"
   - "up" or "down (off or rebooting)"
   - "networking" or "no networking"
   - "display manager" or "no display manager"
      - this line of thinking can extend to other aspects of the system such as `getty`s, `sshd` and so forth

 - Debian is the same too but has `systemd`ish names for things and seems not to have a
   separate "networking enabled" runlevel:

   - 0: poweroff.target
   - 1: rescue.target (not single user - but does single-user *de facto* mean rescue?)
   - 2, 3, 4: multi-user.target
   - 5: graphical.target
   - 6: reboot.target

Let's think of them as kind of analogous to `apt` virtual packages: you have a "virtual
runlevel" for "multi-user support", for "networking", for "defaultroute available", for
"graphical login" and so on, plus packages for bundles of such features.

Perhaps this is what the directory `/run/syndicate/config` could be used for. To switch
"runlevels", you'd symlink (? or otherwise mention) the new runlevel in
`/run/syndicate/config`, and then `rm` the old one.

Maybe draw on Apache 2 config directories, with `conf-available` and `conf-enabled` (if
symlinks are supported).

There should probably be some way of querying for the set of running services that aren't
depended on by anything; once they're all gone, and `poweroff` has been established, we're good
to cut the power.

Relatedly, maybe offer:

    ? <not-running SERVICENAME> [ ... ]

or even

    ? <not PATTERN> [ ... ]

... it'd work for telling the splash screen to go away before launching the rescue root shell,
at least.

Maybe there could be like "radio buttons" for various things? For the conflict handling:
systemd wants `emergency` and `rescue` to conflict, and `rescue` and `multi-user` to conflict
etc.

Looking at my development debian host:

 - poweroff.target:
    - in itself, seems to just be telling `plymouth` to display something pretty to look at,
      and to change its directory away from `/` so that (presumably) the root dir unmount will
      succeed shortly
    - requires & runs after `systemd-poweroff.service`
      - "SuccessAction=poweroff-force"
      - requires & runs after `shutdown.target`, `umount.target`, `final.target`
        - various things hook themselves into the deps of `shutdown.target`. Like, quite a few
        - (it isn't just poweroff that depends on these three: systemd-reboot, -poweroff, -kexec, -halt, -exit all do)
        - `umount.target` is a special internal unit that unmounts all filesystems
        - `final.target` is described as "late shutdown services"; runs after shutdown and umount

 - rescue.target:
    - requires & runs after `sysinit.target` and `rescue.service`
      - sysinit.target:
        - wants & runs after `local-fs.target`, `swap.target`
        - conflicts (??) with `emergency.service` and `emergency.target`, which it also runs after (?)
        - phew, this is a big one.
          - Lots of things hook themselves into `Before` it
          - It also has lots of `wants` (in a directory)
          - it's probably closest to the "core" or "system-layer" milestone I've been playing with
      - rescue.service:
        - a root shell. Tells plymouth to remove the splash screen first

 - multi-user.target:
    - conflicts with `rescue.target` and `rescue.service`
    - requires & runs after `basic.target`
    - lots of daemons mark themselves `WantedBy` it, e.g. apache2, nmbd, daemontools (!), sshd,
      ModemManager, wpa_supplicant, NetworkManager, docker, anacron, cron, dnsmasq, powertop,
      etc.
    - it has a `wants` directory: dbus, getty, logind, user sessions, stuff like that

 - graphical.target:
    - requires and runs after `multi-user`; conflicts w `rescue` explicitly (why not depend on
      multi-user for that?)
    - wants `display-manager.service`

 - reboot.target requires and runs after `systemd-reboot.service`; difference to the `poweroff`
   stuff is just the `SuccessAction`s

# Synit Runlevels

For a phone, I don't think single/multi user makes sense. (Aside from ideas of "switchable
personas", like Android, which I think need support at a different level.) Actually most
runlevels don't make a lot of sense; not yet, at any rate.

Rethink runlevels to mean bundles of active services. There's really only one, for now: "the
phone is ready to use". It will likely make sense to allow the kernel command-line arguments to
pick a default bundle, once more than one exists.

The "rescue" runlevel seems a bit odd in the cellphone setting, but could be a root shell on
console and perhaps also a PostmarketOSesque sshd on usb0. It makes more sense where there's a
user interface able to do some rescuing. (Should it depend on networking? I think I'll leave it
for a later round of development.)

`<milestone system-layer>`:
 - configdirs
 - eudev
 - hostname
 - `fsck`, `mount -a` or equivalent
 - the various dataspaces e.g. machine dataspace
 - everything here is marked `<system-layer-service _>`

`<milestone network>`:
 - the interfaces and routes are being actively scanned
 - interfaces are up, or coming up
 - manages `<internet @via ConnectionClass>` assertion

`<runlevel up>`:
 - has everything else?

`<runlevel down>`:
 - for preparing for shutdown
 - once everything is stable (in state `complete`, `failed` or `ready`) and there are no
   unrequited services, triggers the final unmounts, syncs, and poweroff/reboot actions.

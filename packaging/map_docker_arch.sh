#!/bin/sh
case $1 in
    aarch64) echo arm64;;
    x86_64) echo x86_64;;
    armv7) echo armhf;;
    *)
        echo 'ERROR: Unknown Alpine ARCH '"$1" >&2
        exit 1
esac

#!/bin/sh

# Stomp on installed stuff to get latest dev versions on to a running machine without too much fuss

set -eu

HOST=${1:-pm-qemu}
SUDOPASS=${SUDOPASS:-user}

ssh ${HOST} "
    cp `which rsync` root-rsync;
    echo ${SUDOPASS} | sudo -S sh -c 'chown root:root root-rsync; chmod +s root-rsync';
"
rsync -avu --rsync-path="./root-rsync" \
      packaging/packages/synit-config/files/. \
      ${HOST}:/
rsync -avu --rsync-path="./root-rsync" \
      protocols/schema-bundle.bin \
      ${HOST}:/usr/share/synit/schemas/schema-bundle.prb
rsync -avu --rsync-path="./root-rsync" \
      protocols/schemas/*.prs \
      ${HOST}:/usr/share/synit/schemas/

# ssh ${HOST} "
#     echo ${SUDOPASS} | sudo -S sh -c 'echo \"! <restart-service <daemon <wifi-daemon \\\"wlan0\\\">>>\" > /run/etc/syndicate/services/throck.pr';
# "

# ssh ${HOST} "
#     echo ${SUDOPASS} | sudo -S sh -c 'echo \"! <restart-service <daemon user-settings-daemon>>\" > /run/etc/syndicate/services/throck.pr';
# "

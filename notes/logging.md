---
---

from `ioctl_tty(2)`:

       Redirecting console output
           TIOCCONS
                  Argument: void

                  Redirect output that would have gone to /dev/console or
                  /dev/tty0 to the given terminal.  If that was a
                  pseudoterminal master, send it to the slave.  In Linux
                  before version 2.6.10, anybody can do this as long as the
                  output was not redirected yet; since version 2.6.10, only
                  a process with the CAP_SYS_ADMIN capability may do this.
                  If output was redirected already, then EBUSY is returned,
                  but redirection can be stopped by using this ioctl with fd
                  pointing at /dev/console or /dev/tty0.

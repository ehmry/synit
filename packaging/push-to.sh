#!/bin/sh
if [ -z "$1" ]
then
    echo 'Usage: push-to.sh <HOSTNAME>' >&2
    exit 1
fi
exec rsync -av --delete target/packages $1:.

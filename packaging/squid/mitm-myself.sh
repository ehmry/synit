#!/bin/sh

apk add ca-certificates openssl
openssl s_client -showcerts -connect "$1":3127 </dev/null 2>/dev/null \
    | openssl x509 | tee /usr/local/share/ca-certificates/synit-squid-snakeoil.crt
update-ca-certificates

apk add py3-pip
pip config --global set global.cert /etc/ssl/certs/ca-certificates.crt

#!/bin/sh
#
# Originally loosely based on pack-image.sh from the FRμIT project,
# https://fruit-testbed.org/, which is CC Attribution-ShareAlike 4.0
# licensed.
#
# Run this script after the packages are available in
# apks/target/packages.

set -xe

INITRAMFS=${INITRAMFS:-initramfs-syndicate-system}
NATIVE_COMMANDS=${NATIVE_COMMANDS:-no}
DOCKER_PLATFORM=${DOCKER_PLATFORM:-linux/arm64}
ALPINE_VERSION=${ALPINE_VERSION:-3.14}
VERSION=${VERSION:-0.1.0}
MOUNTPOINT=${MOUNTPOINT:-`pwd`/newroot}

# docker run --rm --name squid adricu/alpine-squid
DOCKER_PROXY_ARGS=${DOCKER_PROXY_ARGS:- --link squid -e http_proxy=http://squid:3128/}

PACKAGES=${PACKAGES:-}

die () {
    echo "$1" >&2
    exit 1
}

die_if_empty () {
    [ ! -z "$2" ] || die "No $1 specified."
}

die_if_empty PACKAGES "$PACKAGES"
die_if_empty VERSION "$VERSION"

docker pull --platform ${DOCKER_PLATFORM} alpine:${ALPINE_VERSION}

invoke_command () {
    # Shell quoting is a nightmare. Beware spaces.
    #
    if [ "$NATIVE_COMMANDS" = "yes" ]
    then
        $@
    else
        tmpscript=$(mktemp tmpscript.XXXXXXXXXX)
        echo "$@" > $tmpscript
        docker run -it --rm -v `pwd`:`pwd` \
               ${DOCKER_PROXY_ARGS} \
               --platform ${DOCKER_PLATFORM} \
               alpine:${ALPINE_VERSION} \
               /bin/sh -c "cd `pwd`; sh $tmpscript"
        rm -f $tmpscript
    fi
}

###########################################################################

TMP_REPO_FILE=$(mktemp repositories.XXXXXXXXXX)

cleanup () {
    # rm -rf ${MOUNTPOINT} || true
    rm -f ${TMP_REPO_FILE} || true
}

trap cleanup 0

cat < /dev/null > ${TMP_REPO_FILE}
echo "`pwd`/apks/target/packages" >> ${TMP_REPO_FILE}
echo "https://dl-cdn.alpinelinux.org/alpine/v${ALPINE_VERSION}/main" >> ${TMP_REPO_FILE}
echo "https://dl-cdn.alpinelinux.org/alpine/v${ALPINE_VERSION}/community" >> ${TMP_REPO_FILE}

# Create root file system.
#
echo "Repositories:"
cat ${TMP_REPO_FILE}
apkcmd="apk --repositories-file `pwd`/${TMP_REPO_FILE} -U --allow-untrusted"
invoke_command ${apkcmd} --root ${MOUNTPOINT} --initdb add ${PACKAGES}
# invoke_command ${apkcmd} --no-script --root ${MOUNTPOINT} add mkinitfs

# Allow root logins on ttyS0 / ttyAMA0
#
tmpsecuretty=$(mktemp tmpsecuretty.XXXXXXXXXX)
cat ${MOUNTPOINT}/etc/securetty | egrep -v '(ttyS0|ttyAMA0)' > $tmpsecuretty
echo ttyS0 >> $tmpsecuretty
echo ttyAMA0 >> $tmpsecuretty
cat $tmpsecuretty > ${MOUNTPOINT}/etc/securetty
rm -f $tmpsecuretty

cat > ${MOUNTPOINT}/etc/os-release <<EOF
VERSION=${VERSION}
PRETTY_NAME="syndicate-system v${VERSION}"
BUILT_TIMESTAMP=$(date +%s)
COMMIT=$(git rev-parse HEAD)
EOF

cp -a syndicate-server ${MOUNTPOINT}/sbin/.
cp -a synit-pid1 ${MOUNTPOINT}/sbin/.
cp -a preserves-tool ${MOUNTPOINT}/usr/bin/.
cp -a etc_syndicate.tar.gz ${MOUNTPOINT}/.

cat > ${MOUNTPOINT}/etc/apk/repositories <<EOF
https://dl-cdn.alpinelinux.org/alpine/v${ALPINE_VERSION}/main
https://dl-cdn.alpinelinux.org/alpine/v${ALPINE_VERSION}/community
EOF

cp package-list ${MOUNTPOINT}/package-list
cp init.sh ${MOUNTPOINT}/init
chmod a+x ${MOUNTPOINT}/init

cp -a ${MOUNTPOINT}/boot/vmlinuz-virt .

(cd ${MOUNTPOINT}; find . -print0 | \
     cpio --null --create --verbose --format=newc) | \
    gzip > ${INITRAMFS}

trap - 0
cleanup

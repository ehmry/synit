 - signals (SIGTERM)

 - cron
 - at

 - user settings:
   - ✓ wifi networks
   - ✓ wwan enabled/disabled, plus apn
   - screen brightness level

 - squeak

   - ✓ scrolling by dragging
   - pinch zoom

   - ✓ demo app that opens the map

   - ✓ phone call audio

   - terminal that isn't based on a transcript??

   - ✓ blank screen on/off with power button
     - ✓ stop listening to screen, too!

   - sms UI
     - ✓ scrollable lists
     - ✓ big lists
     - browse SMS database

 - power management

   - sleep mode (!!!)
   - wake up periodically??
   - note that will need to re-initialize the framebuffer to stop the cursor from appearing,
     screensaver operating, etc.

 - get firefox running in vnc? (Xvfb -> x11vnc -> squeak vnc client ...? Keyboard focus is
   wonky and firefox isn't set up right, but the basics work already)

 - docker
   - appears to need `CONFIG_BPF_SYSCALL`, which is not set in the stock kernel

use nix::sys::socket;
use nix::sys::uio::IoVec;

use std::io;

fn main() -> io::Result<()> {
    let sock = socket::socket(socket::AddressFamily::Netlink,
                              socket::SockType::Raw,
                              socket::SockFlag::SOCK_CLOEXEC,
                              socket::SockProtocol::NetlinkKObjectUEvent)?;

    let src = socket::SockAddr::new_netlink(0, 1 /* is this the group for kernel-announced events? */);
    socket::bind(sock, &src)?;

    loop {
        println!("Waiting...");
        let mut buf = vec![0u8; 8192];
        let iov = [IoVec::from_mut_slice(&mut buf)];
        let mut cmsg = Vec::with_capacity(8192);
        let r = socket::recvmsg(sock, &iov, Some(&mut cmsg), socket::MsgFlags::MSG_CMSG_CLOEXEC)?;
        println!("\n{:#?}", r);
        for i in 0 .. r.bytes {
            let b = buf[i];
            if b == 0 {
                println!();
            } else if b >= 32 && b < 128 {
                print!("{}", b as char);
            } else {
                print!("\\x{:02x}", b);
            }
        }
        println!();
    }
}

ARG DOCKER_ARCH
FROM --platform=linux/${DOCKER_ARCH} alpine:edge

# Instead of doing the following, we add a squid cert to effectively MITM ourselves (!):
#   RUN sed -i -e s:https:http:g /etc/apk/repositories
#
ARG http_proxy_hostname
COPY ./squid/mitm-myself.sh /root
# This step is just to force rerun of the mitm config when the key changes,
# e.g. when the squid image is rebuilt.
COPY ./squid/squid-ca.pem /tmp
RUN /root/mitm-myself.sh ${http_proxy_hostname}

ARG http_proxy
RUN http_proxy=${http_proxy} https_proxy=${http_proxy} apk add bash sudo alpine-sdk linux-headers

ARG UID
ARG BUILD_USER
RUN adduser -D -u ${UID} ${BUILD_USER} && addgroup ${BUILD_USER} abuild
RUN echo "${BUILD_USER} ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers

ARG KEYFILE
COPY ${KEYFILE}.pub /etc/apk/keys/.
RUN mkdir -p /home/${BUILD_USER}/.abuild/
RUN echo "PACKAGER_PRIVKEY=/home/${BUILD_USER}/.abuild/${KEYFILE}" >> /home/${BUILD_USER}/.abuild/abuild.conf

USER ${BUILD_USER}
WORKDIR /data

# On Syndicate Configuration

inotify doesn't go through symlinks, making symlinks a little less useful than they could
otherwise be, which is a shame.

HOWEVER the initial scan DOES go through symlinks. This is a bug. Either make the reactive code
traverse symlinks (would be nice??), or make the initial scan ignore symlinks.

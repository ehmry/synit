; Pinephone:
<require-service <qmi-wwan "/dev/cdc-wdm0">>

<depends-on <qmi-wwan "/dev/cdc-wdm0"> <service-state <daemon eg25-manager> up>>

<daemon eg25-manager "eg25-manager">
<depends-on <daemon eg25-manager> <service-state <daemon eg25-manager-monitor> up>>
<daemon eg25-manager-monitor "/usr/lib/synit/eg25-manager-monitor">

; More generally:

? <user-setting <mobile-data-enabled>> [
  ? <user-setting <mobile-data-apn ?apn>> [
    ? <run-service <qmi-wwan ?dev>> [
      <require-service <daemon <qmi-wwan-manager $dev $apn>>>
      $log ! <log "-" { line: "starting wwan manager", dev: $dev, apn: $apn }>
    ]
  ]
]

? <run-service <daemon <qmi-wwan-manager ?dev ?apn>>> [
  <daemon <qmi-wwan-manager $dev $apn> ["/usr/lib/synit/qmi-wwan-manager" $dev $apn]>
  ; Occasionally qmi-wwan-manager complains on stderr about the link going away, but doesn't
  ; terminate. Fortunately it doesn't seem to talk on stderr at all unless the link goes away,
  ; so we interpret any stderr activity (!) as indication of a problem here.
  $log ?? <log _ {
    service: <daemon <qmi-wwan-manager $dev $apn>>
    stream: stderr
    pid: ?pid
  }> [
    ; We used to just do this:
    ;
    ; $config ! <restart-service <daemon <qmi-wwan-manager $dev $apn>>>
    ;
    ; ... but it immediately restarts the service, causing lots of pointless load and log spam.
    ; TODO: make it so you can include an exit status in the restart request; something like
    ; "restart immediately" vs "simulated normal termination" vs "simulated error termination",
    ; triggering the appropriate supervisor behaviours.
    ;
    ; So instead we kill the qmicli program with a signal, which *does* trigger the supervisor
    ; in the right way.
    let ?pid = stringify $pid
    $config ! <exec ["kill", "-INT", $pid] never>
  ]
]

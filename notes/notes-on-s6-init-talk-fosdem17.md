---
title: "Notes on Laurent Bercot's talk at FOSDEM 2017"
---

Talk more about s6 and init than s6 and supervision.

We can get to 100% satisfaction with an init system. Lots of people
are still unhappy with their init system though.

This talk is about design of an init system.

4 elements:

 - /sbin/init (the startup scripts/programs/processes)
 - PID 1
 - process supervision
 - service management

These 4 elements don't have to be in the same process.

02:55

Process supervision: supervision tree, rooted at pid1.

"Supervision only applies to daemons" - huh, interesting

Service management: bring services up/down; More generally, change
from one state to another.

Services: oneshots (short-lived, with side-effects) or longruns
(daemons). Have dependencies (enforced by service manager).

 - Ah, a oneshot might be a traditional-init-like "run a program to
   set things up" so that the service is "running" even though there's
   no particular process you can point to that *is* the service?

    - 10:30 - Void Linux has only longruns (?) and so "hacks",
      "longruns that do nothing" are used to emulate oneshots

05:40

Ah this is an interesting perspective. Bercot classifies various
systems in terms of the 4 elements:

                                   /sbin/init   pid1   supervision   svcmgr
    Integrated init systems
    (systemd, launchd, upstart)        ✓         ✓          ✓          ✓

    sysvinit, BSD init                 ✓         ✓          ✓
      - sysv-rc, /etc/rc                                                ✓

    OpenRC                                                              ✓

08:10

Daemontools family - daemontools (1998!), daemontools-encore, runit,
perp, s6 - supervision suites; nosh - suite of tools similar to s6

09:55

Supervision suites are not enough - only 1/4 of an init system.

11:40

s6: technical aspects

"reproducible launch environment" even for oneshots
